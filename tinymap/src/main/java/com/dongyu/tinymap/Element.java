package com.dongyu.tinymap;

import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Point;
import ohos.media.image.PixelMap;

public class Element extends PixelMapHolder {

    private Point mercatorPoint = new Point(0,0);

    private Point originPoint = new Point(0,0);
    private Point nowPoint = new Point(0,0);

    public Element(PixelMap pixelMap) {
        super(pixelMap);
    }

    public Element(PixelMap pixelMap, float x, float y) {
        super(pixelMap);
        originPoint = new Point(x, y);
        nowPoint = new Point(x, y);
    }


    public float getX() {
        return nowPoint.getPointX();
    }

    public void setX(float x) {
        nowPoint = new Point(x, nowPoint.getPointY());
    }

    public float getY() {
        return nowPoint.getPointY();
    }

    public void setY(float y) {
        nowPoint = new Point(nowPoint.getPointX(), y);
    }

    public float getOriginX() {
        return originPoint.getPointX();
    }

    public void setOriginX(float x) {
        originPoint = new Point(x, originPoint.getPointY());
    }

    public float getOriginY() {
        return originPoint.getPointY();
    }

    public Point getMercatorPoint() {
        return mercatorPoint;
    }

    public void setMercatorPoint(Point mercatorPoint) {
        this.mercatorPoint = mercatorPoint;
    }

    public void setOriginY(float y) {
        originPoint = new Point(originPoint.getPointX(), y);
    }

    public Point getOriginPoint() {
        return originPoint;
    }

    public void setOriginPoint(Point originPoint) {
        this.originPoint = originPoint;
    }

    public Point getNowPoint() {
        return nowPoint;
    }

    public void setNowPoint(Point nowPoint) {
        this.nowPoint = nowPoint;
    }
}
