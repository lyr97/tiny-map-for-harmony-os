package com.dongyu.tinymap;

import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Point;
import ohos.media.image.PixelMap;

public class Tile extends PixelMapHolder {

    private Point originPoint = new Point(0,0);
    private Point nowPoint = new Point(0,0);

    private int row;
    private int col;

    public Tile(PixelMap pixelMap) {
        super(pixelMap);
    }

    public Tile(PixelMap pixelMap, float x, float y) {
        super(pixelMap);
        originPoint = new Point(x, y);
        nowPoint = new Point(x, y);
    }

    public float getX() {
        return nowPoint.getPointX();
    }

    public void setX(float x) {
        nowPoint = new Point(x, nowPoint.getPointY());
    }

    public float getY() {
        return nowPoint.getPointY();
    }

    public void setY(float y) {
        nowPoint = new Point(nowPoint.getPointX(), y);
    }

    public float getOriginX() {
        return originPoint.getPointX();
    }

    public void setOriginX(float x) {
        originPoint = new Point(x, originPoint.getPointY());
    }

    public float getOriginY() {
        return originPoint.getPointY();
    }

    public void setOriginY(float y) {
        originPoint = new Point(originPoint.getPointX(), y);
    }


    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public boolean isInBoundary(int rowmin, int rowmax, int colmin, int colmax) {
        if (row < rowmin || row > rowmax || col < colmin || col > colmax) {
            return false;
        }
        return true;
    }
}
